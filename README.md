# ProxMox VE 8.1.4
This project contains the necessary files to automate and configure instances on my ProxMox VE 8.1.4 server.

The instances are created using Terraform files, while the configuration is done through Ansible playbooks.


# Links
- [ProxMox Server setup](#my-setup)
- [Ansible Installation](#ansible-installation)
- [Terraform(OpenTofu) Installation](#terraformopentofu-installation)

<br>

# My setup
This is my personal setup for my Proxmox server. It is not intended to be a general-purpose solution, but it can serve as a starting point for your own setup.

To partition your disks, follow these steps:
- SSD (512GB):
  - 16GB swap
  - 150GB for root
  - Remaining storage for containers and VMs
- HDD (1TB):
  - Format as ext4

To format a drive, follow these steps:

1. Find the drive using either `sudo fdisk -l` or `sudo lsblk` (for me it's `/dev/sda`).
2. Use the command `sudo dd if=/dev/zero of=/dev/sda bs=4M status=progress` to wipe the drive.
3. Reboot the system to apply the changes.
4. Verify the drive using `sudo lsblk`.

To create partitions, follow these steps:

1. Use the command `sudo fdisk /dev/sda` to start partitioning.
2. Type `n` for a new partition.
3. Choose the partition type (primary or extended).
4. Specify the partition number, first sector, and last sector.
5. Type `w` to save the changes.
6. Verify the partitions using `lsblk` or `sudo fdisk -l | grep diskname`.

To mount the disk:

1. Create a folder for mounting, e.g., `/mnt/storage`.
2. Format the disk using `sudo mkfs.ext4 devicename`.
3. Mount the disk using `sudo mount devicename /mnt/storage`.
   - Note: To make the mount permanent, add an entry to `/etc/fstab`.
4. Verify the mount using `sudo mount -a`.

Once again, this is my personal setup. You can use your own setup. <br>
After mounting the disk, access the Proxmox web UI and go to Datacenter -> Storage -> Add.

- For the ID, enter a name for the storage.
- For the Directory, enter the mount path.
- For the Content, select "Disk image and Containers".

User permissions: <br>
Proxmox does not allow a non-root user to deploy LXC containers. Therefore, you have to use root@pam to deploy containers. However, you can use a non-root user to deploy VMs.

For container templates:

- CLI:
  - Update templates using `pveam update`.
  - List available templates using `pveam available > templates.txt` (This will create a file called templates.txt with the list of available templates).
  - Download a template using `pveam download [storage] [template_name]`.
- GUI:
  - Select the storage disk for container templates.
  - Go to Templates and select a template.

<br>

# Ansible installation
To use Ansible, you need to install it on your local machine. You can install it using the following command:
```bash
sudo apt-add repository ppa:ansible/ansible
sudo apt install ansible -y
```

For Red-Hat based systems:
```bash
sudo dnf install ansible -y or sudo yum install ansible -y
```

Next, you need to install proxmoxer and requests as pip packages:
```bash
pip install proxmoxer requests
```

Tags are used to run specific tasks. All the tags that i use are defined in `site.yml` or `main.yml` of each role. To run a specific tag, use the following command:
```bash
./run -l {limit} -t {tag}
```
> **Limit** is the host or group of hosts you want to run the tag on. <br>
> **Tag** is the specific task you want to run.

To run all the tasks of a specific role, use the following command:
```bash
./run -l {limit} -t all
```

# Terraform(OpenTofu) Installation
I am using an open-source project called OpenTofu to manage my Proxmox instances. You can find the project at: [https://opentofu.org](https://opentofu.org)

In order to deploy instances using OpenTofu, you need to initialize a project within the ~/terraform/{instance-type} directory. You can do this by running the following command:
```bash
tofu init
```

To view the changes that will be made to the Proxmox server, run the following command:
```bash
tofu plan
```

To apply the changes, run the following command:
```bash
tofu apply
```

To destroy the instances, run the following command:
```bash
tofu destroy
```