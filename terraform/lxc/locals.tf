locals {
    lxc_yml_config = file("${path.module}/${var.lxc_yml_path}")
    lxc_config = yamldecode(local.lxc_yml_config)
}