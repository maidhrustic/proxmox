provider "proxmox" {
  pm_api_url = var.pm_api_url
  pm_api_token_secret = var.pm_api_token_secret
  pm_api_token_id = var.pm_api_token_id
  pm_tls_insecure = true
}

resource "proxmox_lxc" "lxc" {
    for_each = { for idx, lxc_containers in local.lxc_config[ "lxc_containers" ] : idx => lxc_containers }

    target_node = "pve"
    vmid = each.value.vmid
    hostname = each.value.name
    unprivileged = true
    ostemplate = each.value.ostemplate
    cores = each.value.cores
    memory = each.value.memory
    password = var.container_password
    onboot = false
    start = false
    ssh_public_keys = <<EOT
            ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEtb/OBuhhFWg6a+D7dGCQlNW97TdXtFlzJsBMOO92oN MaidPC-WSL
        EOT

    rootfs {
        size = each.value.size
        storage = "HDD"
    }

    network {
        ip = each.value.network_ip
        bridge = each.value.network_bridge
        name = each.value.interface_name
        gw = each.value.network_gw
        firewall = true
    }
}