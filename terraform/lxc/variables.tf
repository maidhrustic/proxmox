variable "lxc_yml_path" {
	description = "Path to the LXC configuration yml file"
	type	= string
	default	= "../../host_vars/pve.yml"
}

variable "pm_api_url" {
	description = "Proxmox API URL"
	type = string
	default = "https://pve.famhrustic.nl:8006/api2/json"
}

variable "pm_api_token_secret" {
	description = "Proxmox API Token Secret"
	type = string
	default = "1cfa1bc0-b284-470f-b55e-512514ab6315"
}

variable "pm_api_token_id" {
	description = "Proxmox API Token ID"
	type = string
	default = "terraform@pve!provider"
}

variable "container_password" {
	description = "Password for the LXC containers"
	type = string
	default = "PASSWORD HERE" # change
	sensitive = true
}